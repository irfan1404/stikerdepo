<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kontroldata extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('globalmodel');
    $this->load->library('m_pdf');
    if(!$this->session->userdata('logged_in')){
      redirect('login');
    }
  }

  function index(){  
      $url = $this->uri->segment(1);
      $access = access_menu($url);
      $data['access']     = $access;
      $data['title']      = "Kontrol Data";
      
      if($access['read'] == 0){  $this->load->view('view_denied'); }
      if($access['read'] == 1){  $this->load->view('kontroldata/view_template',$data); }
  }

  public function load_data2(){
    $url = $this->uri->segment(1);
    $access = access_menu($url);
    $data['access'] = $access;
    $data['data']   = $this->globalmodel->getDataArray('menu_group');
    $data['list']   = $this->globalmodel->getdetailarray();
    $this->load->view('kontroldata/view_list',$data);
  }

  function outlet_all(){

     $where = array(
      'KdCabang' => $this->session->userdata('KdCabang')
    );
    $result = $this->globalmodel->getdetailkdoutlet($where);
    echo json_encode(array('message'=>'success','return'=>$result));
  }

  function dataonleneall(){
    $id=$this->input->post('id');
    $where = array(
     'KdCabang' => $this->session->userdata('KdCabang')
    );
    $dataktk    = $this->globalmodel->datakontrol(array('nokontrol'=>$id),'array');
    $result = $this->globalmodel->getdetailkdoutlet($where);
   echo json_encode(array('message'=>'success','return'=>$result,'kontrol'=>$dataktk));
 }

  function save(){
    $nokontrol    = $this->getUrutOutlet();
    $KdOutlet     = $this->input->post('kodeoutlet');
    $action       = $this->input->post('action');
    if($action=="simpan"){
      if(!empty($nokontrol)){
        $ceknilai = $this->globalmodel->datakontrol(array('nokontrol'=>$nokontrol),'nums');
        if($ceknilai==0){
          $ceknilioutlet = $this->globalmodel->datakontrol(array('KdOutlet'=>$KdOutlet),'array');
          if(!empty($ceknilioutlet)){
            echo json_encode(array('message'=>'gagal'));die;
          }
          $KdCabang   = $this->session->userdata('KdCabang');
          $where    = array(
            'a.KdOutlet'        => $KdOutlet,
            'b.TrueLocation'    => 'YES',
            'a.KdCabang'        => $KdCabang
          );
            $datadepo   = $this->globalmodel->dataall($where,$KdCabang);
            if(empty($datadepo)){
              echo json_encode(array('message'=>'kosong'));die;
            }
            $KdOutlet   = $datadepo[0]['KdOutlet'];
            $Nama       = $datadepo[0]['Nama'];
            $Alm1Toko   = $datadepo[0]['Alm1Toko'];
            $KotaToko   = $datadepo[0]['KotaToko'];
            $Longitude  = $datadepo[0]['Longitude'];
            $Latitude   = $datadepo[0]['Latitude'];
            $KdCabang   = $datadepo[0]['KdCabang'];
            $datenow    = date('Y-m-d H:i:s');  
            $data= array(
              'Longitude'   => $Longitude,
              'Latitude'    => $Latitude,
              'KdCabang'    => $KdCabang,
              'adddate'     => $datenow,
              'nokontrol'   => $nokontrol,
              'Nama'        => $Nama,
              'AltToko'     => $Alm1Toko,
              'KotaToko'    => $KotaToko,
              'KdOutlet'    => $KdOutlet,
              'KdCabang'    => $this->session->userdata('KdCabang'),
              'adduser'    => $this->session->userdata('username')
            ); 
            
            $id = $this->globalmodel->insertData('kontroldata',$data);
            $id = $this->db->insert_id();
            $this->printadds($id);

          echo json_encode(array('message'=>'success'));
        }else{
          echo json_encode(array('message'=>'gagal'));
        }
      }
    }else{
        $nokontrol= $this->input->post('id');
        $Nama     = $this->input->post('Nama');
        $Alamat   = $this->input->post('Alamat');
        $Kota     = $this->input->post('Kota');
        $data_edit = array(
          'Nama'        => $Nama,
          'AltToko'     => $Alamat,
          'KotaToko'    => $Kota
        ); 
        $this->globalmodel->editData(array('nokontrol'=>$nokontrol),$data_edit,'kontroldata');
        echo json_encode(array('message'=>'success'));
    }
  
  }

  function edit(){
    $id = $this->input->post('id');
    $dataktk = $this->globalmodel->datakontrol(array('nokontrol'=>$id));
    $dataoutlet = $this->globalmodel->dataoutlet();
  }

  function delete(){
    $id = $this->input->post('id');
      $delete = $this->globalmodel->deleteData('kontroldata',array('nokontrol'=>$id));
      if($delete){
          echo json_encode(array('message'=>'success'));
      }else{
          echo json_encode(array('message'=>'failed'));
      }
  }

  function printadds($id){
    $dataktk    = $this->globalmodel->datakontrol(array('nokontrol'=>$id),'array');
    $nokontrol  = $dataktk[0]['nokontrol'];
    $Nama       = $dataktk[0]['Nama'];
    $KdOutlet   = $dataktk[0]['KdOutlet'];
    $AltToko    = $dataktk[0]['AltToko'];
    $KotaToko   = $dataktk[0]['KotaToko'];
    $Longitude  = $dataktk[0]['Longitude'];
    $Latitude   = $dataktk[0]['Latitude'];
    $KdCabang   = $dataktk[0]['KdCabang'];
    $print      = $dataktk[0]['print'];
    $this->load->library('ciqrcode'); 
    $config['cacheable']    = true; 
    $config['cachedir']     = './assets/'; 
    $config['errorlog']     = './assets/'; 
    $config['imagedir']     = './assets/upload/'; 
    $config['quality']      = true; 
    $config['size']         = '1024'; 
    $config['black']        = array(224,255,255); 
    $config['white']        = array(70,130,180); 
    $this->ciqrcode->initialize($config);
    $image_name = $nokontrol.'.png';

      //CARA 1
      $codeContents = $Longitude."|";
      $codeContents .= $Latitude."|";
      $codeContents .= $KdCabang."|";
      $codeContents .= $KdOutlet;

    $params['data']     = $codeContents;
    $params['level']    = 'H';
    $params['size']     = 100;
    $params['savename'] = FCPATH.$config['imagedir'].$image_name;
    $this->ciqrcode->generate($params); 
    $this->db->update('kontroldata',array('nama_gambar'=>$image_name),array('nokontrol'=>$nokontrol));

  }

  function printadd(){
    $id         = $this->input->post('id');
    $dataktk    = $this->globalmodel->datakontrol(array('nokontrol'=>$id),'array');
    $nokontrol  = $dataktk[0]['nokontrol'];
    $Nama       = $dataktk[0]['Nama'];
    $KdOutlet   = $dataktk[0]['KdOutlet'];
    $AltToko    = $dataktk[0]['AltToko'];
    $KotaToko   = $dataktk[0]['KotaToko'];
    $Longitude  = $dataktk[0]['Longitude'];
    $Latitude   = $dataktk[0]['Latitude'];
    $KdCabang   = $dataktk[0]['KdCabang'];
    $print      = $dataktk[0]['print'];
    $this->load->library('ciqrcode'); 
    $config['cacheable']    = true; 
    $config['cachedir']     = './assets/'; 
    $config['errorlog']     = './assets/'; 
    $config['imagedir']     = './assets/upload/'; 
    $config['quality']      = true; 
    $config['size']         = '1024'; 
    $config['black']        = array(224,255,255); 
    $config['white']        = array(70,130,180); 
    $this->ciqrcode->initialize($config);
    $image_name = $nokontrol.'.png';

      //CARA 1
      $codeContents = $Longitude."|";
      $codeContents .= $Latitude."|";
      $codeContents .= $KdCabang."|";
      $codeContents .= $KdOutlet;

    $params['data']     = $codeContents;
    $params['level']    = 'H';
    $params['size']     = 100;
    $params['savename'] = FCPATH.$config['imagedir'].$image_name;
    $this->ciqrcode->generate($params); 
    $data['gambar']     = $image_name;
    $data['KdOutlet']   = $KdOutlet;
    $data['Nama']       = $Nama;
    $data['AltToko']    = $AltToko;

    $this->load->view('kontroldata/view_modal_canvas',$data);

  }

  function printcobakesekiankali(){
    $this->load->view('kontroldata/view_print');
  }

  function getUrutOutlet(){
    $bulan			  =  date('m');
    $tahun			  =  date('Y');
    $getLastno		= $this->globalmodel->getselect("nokontrol from kontroldata where MID(nokontrol,5,6)='$tahun$bulan' order by nokontrol desc ");

    if (!empty($getLastno)){
      $dtlastno	= $getLastno[0]['nokontrol'];
      $getlastno	= substr($dtlastno,11,3);
      $nofcs		= $getlastno + 1;
      if (strlen($nofcs)=='1'){
        $counter	= "00".$nofcs;
      }elseif (strlen($nofcs)=='2'){
        $counter	= "0".$nofcs;
      }else{
        $counter	= $nofcs;
      }			
    }else {
      $counter	= "001";
    }
    $nofcs		= "KTK-".$tahun.$bulan."-".$counter;
    return $nofcs;
  }

  function printalloutlet(){
    $this->load->view('kontroldata/view_modal_canvas_all');
  }
  
  function printpdf($nokontrol){
    $dataktk    = $this->globalmodel->datakontrol(array('nokontrol'=>$nokontrol),'array');
    $nokontrol  = $dataktk[0]['nokontrol'];
    $Nama       = $dataktk[0]['Nama'];
    $KdOutlet   = $dataktk[0]['KdOutlet'];
    $AltToko    = $dataktk[0]['AltToko'];
    $KotaToko   = $dataktk[0]['KotaToko'];
    $Longitude  = $dataktk[0]['Longitude'];
    $Latitude   = $dataktk[0]['Latitude'];
    $KdCabang   = $dataktk[0]['KdCabang'];
    $print      = $dataktk[0]['print'];
    $this->load->library('ciqrcode'); 
    $config['cacheable']    = true; 
    $config['cachedir']     = './assets/'; 
    $config['errorlog']     = './assets/'; 
    $config['imagedir']     = './assets/upload/'; 
    $config['quality']      = true; 
    $config['size']         = '1024'; 
    $config['black']        = array(224,255,255); 
    $config['white']        = array(70,130,180); 
    $this->ciqrcode->initialize($config);
    $image_name = $nokontrol.'.png'; 
    
    //CARA 1
    $codeContents = $Longitude."|";
    $codeContents .= $Latitude."|";
    $codeContents .= $KdCabang."|";
    $codeContents .= $KdOutlet;

    $params['data']     = $codeContents;
    $params['level']    = 'H';
    $params['size']     = 10;
    $params['savename'] = FCPATH.$config['imagedir'].$image_name;
    $this->ciqrcode->generate($params); 
      
    $data['AltToko'] = $AltToko;
    $data['gambar'] = $image_name;
    $data['list']   = $dataktk;
    $html = $this->load->view('kontroldata/pdf_gambar',$data,true );
    $pdfFilePath = "pdfgambar.pdf";
    $pdf = $this->m_pdf->load();
    $pdf->SetHTMLHeader('<div style="padding-top:10px;font-weight:bold;">PT. Victoria Care Indonesia</div><hr>'); 
    $pdf->SetHTMLFooter('<hr><br><div  style="font-size:11px;"><i>Di cetak oleh: '.$this->session->userdata("username").' '.date('d F Y').' Cabang '.$KdCabang.'</i></div>');
    $pdf->WriteHTML($html);
    $pdf->Output();
    exit;
  }


  function printallpdf(){
    $data_array     = $this->globalmodel->dataallkontrol($this->session->userdata('KdCabang'),'array');

    foreach($data_array as $dta){
      $nokontrol  = $dta['nokontrol'];
      $Nama       = $dta['Nama'];
      $KdOutlet   = $dta['KdOutlet'];
      $AltToko    = $dta['AltToko'];
      $KotaToko   = $dta['KotaToko'];
      $Longitude  = $dta['Longitude'];
      $Latitude   = $dta['Latitude'];
      $KdCabang   = $dta['KdCabang'];
      $print      = $dta['print'];

      $this->load->library('ciqrcode'); 
      $config['cacheable']    = true; 
      $config['cachedir']     = './assets/'; 
      $config['errorlog']     = './assets/'; 
      $config['imagedir']     = './assets/upload/'; 
      $config['quality']      = true; 
      $config['size']         = '1024'; 
      $config['black']        = array(224,255,255); 
      $config['white']        = array(70,130,180); 
      $this->ciqrcode->initialize($config);
      $image_name = $nokontrol.'.png'; 
        //CARA 1
        $codeContents = $Longitude."|";
        $codeContents .= $Latitude."|";
        $codeContents .= $KdCabang."|";
        $codeContents .= $KdOutlet;
  
        $params['data']     = $codeContents;
        $params['level']    = 'H';
        $params['size']     = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name;
        $this->ciqrcode->generate($params); 
        $this->globalmodel->instername('kontroldata',array('nama_gambar'=>$image_name),array('nokontrol'=>$nokontrol));
    }

    $data['list']   = $data_array;
    $html           = $this->load->view('kontroldata/pdfall_gambar',$data,true );// echo $html;die();
    $pdf            = $this->m_pdf->load();
    $pdf->SetHTMLHeader('<div style="padding-top:10px;font-weight:bold;">PT. Victoria Care Indonesia</div><hr>'); 
    $pdf->SetHTMLFooter('<hr><br><div  style="font-size:11px;"><i>Di cetak oleh: '.$this->session->userdata("username").' '.date('d F Y').' Cabang '.$KdCabang.'</i></div>');
    $pdf->WriteHTML($html);
    $pdf->Output();
    exit;
  }

}