<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_menu extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('globalmodel');
        if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
    }

	public function index(){   
        $url = $this->uri->segment(1);
        $access = access_menu($url);
        $data['access'] = $access;
        if($access['read'] == 0){  $this->load->view('view_denied'); }
        if($access['read'] == 1){  $this->load->view('view_template',$data); }
    }

    public function load_data(){
        $url = $this->uri->segment(1);
        $access = access_menu($url);
        $data['access'] = $access;
        $data['data'] = $this->globalmodel->getDataArray('menu_group');
        $this->load->view('view_group_menu',$data);
    }

    public function access()
    {
        
        $url = $this->uri->segment(1);
        $access = access_menu($url);
        $group_id = $this->input->post('group_id');
        $group_name = $this->input->post('group_name');
        
        $data['access'] = $access;  
        $data['data'] = $this->globalmodel->getWhereDataArray('menu_access',array('group_id'=>$group_id));
        $data['group_id'] = $group_id;
        $data['group_name'] = $group_name;
        $this->load->view('view_access_menu',$data);
    }

    public function save_access(){
        $group_id = $this->input->post('group_id');
        $arr_menu_id = $this->input->post('menu_id');
        $arr_view = $this->input->post('view');
        $arr_read = $this->input->post('read');
        $arr_create = $this->input->post('create');
        $arr_update = $this->input->post('update');
        $arr_delete = $this->input->post('delete');
        $arr_approve = $this->input->post('approve');
        $arr_download = $this->input->post('download');
        $arr_print = $this->input->post('print');

        $this->globalmodel->deleteData('menu_access',array('group_id'=> $group_id));
        foreach ($arr_menu_id as $key => $value) {

            $menu_id = $value;
            $view = (isset($arr_view[$menu_id])) ? '1' : '0'; 
            $read = (isset($arr_read[$menu_id])) ? '1' : '0'; 
            $create = (isset($arr_create[$menu_id])) ? '1' : '0'; 
            $update = (isset($arr_update[$menu_id])) ? '1' : '0'; 
            $delete = (isset($arr_delete[$menu_id])) ? '1' : '0'; 
            $approve = (isset($arr_approve[$menu_id])) ? '1' : '0'; 
            $download = (isset($arr_download[$menu_id])) ? '1' : '0'; 
            $print = (isset($arr_print[$menu_id])) ? '1' : '0'; 
            $data = array(
                           'menu_id'  => $menu_id, 
                           'group_id' => $group_id, 
                           'view'     => $view, 
                           'read'     => $read, 
                           'create'   => $create, 
                           'update'   => $update, 
                           'delete'   => $delete, 
                           'approve'  => $approve, 
                           'download' => $download, 
                           'print' => $print
                        ); 
            $this->globalmodel->insertData('menu_access',$data);
           
        }
        echo json_encode(array('message'=>'success'));
    }

    public function save(){
        $name_group = $this->input->post('name_group');
        if(!$this->input->post('id')){
            $insert = $this->globalmodel->insertData('menu_group',array('group_name'=>$name_group));
            if($insert) {
                echo json_encode(array('message'=>'success'));
            }else{
                echo json_encode(array('message'=>'failed'));
            }
        }else{
            $id = $this->input->post('id');
            $update = $this->globalmodel->updateData('menu_group',array('group_name'=>$name_group),array('group_id'=>$id));
            if($update){
                echo json_encode(array('message'=>'success'));
            }else{
                echo json_encode(array('message'=>'failed'));
            }
        }
    }

    public function delete(){
        $id = $this->input->post('id');
        $delete = $this->globalmodel->deleteData('menu_group',array('group_id'=>$id));
        if($delete)
        {
            echo json_encode(array('message'=>'success'));
        }
        else{
            echo json_encode(array('message'=>'failed'));
        }
    }

    function users()
    {
        $url = $this->uri->segment(1);
        $access = access_menu($url);
        $group_id = $this->input->post('group_id');
        $group_name = $this->input->post('group_name');
        
        $data['access'] = $access;  
        $data['data'] = $this->globalmodel->getWhereDataArray('user',array('group_id'=>$group_id));$this->db->last_query();
        $data['group_id'] = $group_id;
        $data['group_name'] = $group_name;
        $this->load->view('view_users',$data);
    }

    function users_all()
    {
        $result = $this->globalmodel->getDataArray('user');
        echo json_encode(array('message'=>'success','return'=>$result));
    }

    function save_user_group()
    {
        $group_id = $this->input->post('group_id');
        $username = $this->input->post('username');
        $update = $this->globalmodel->updateData('user',array('group_id'=>$group_id),array('username'=>$username));
        if($update === TRUE)
        {
            echo json_encode(array('message'=>'success'));
        }
        else{
            echo json_encode(array('message'=>'failed'));
        }
    }

    function delete_from_group()
    {
        $username = $this->input->post('username');
        $update = $this->globalmodel->updateData('user',array('group_id'=>'0'),array('username'=>$username));
        if($update === TRUE)
        {
            echo json_encode(array('message'=>'success'));
        }
        else{
            echo json_encode(array('message'=>'failed'));
        }
    }
}

/* End of file access_menu.php */
/* Location: ./application/controllers/business_plan.php */

















