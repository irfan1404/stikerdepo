<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
        error_reporting(0);
        parent::__construct();
		$this->load->model('globalmodel');
		if(!$this->session->userdata('logged_in')){
			redirect('login');
		}
	}
	
	public function index(){  

        $url = $this->uri->segment(1);
        $access = access_menu($url);
        $directory = get_directory_menu($url);
        $data['access'] = $access;
        if($access['read'] == 0){  $this->load->view('view_denied'); }
        if($access['read'] == 1){  $this->load->view('view_home',$data); }
		
    }

    public function cetak($file){
        // echo $file;die();
        // $upload_dir = "upload/";

        $upload_dir = "assets/print/";
        $img = $_POST['hidden_data'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $upload_dir . mktime() . ".png";
        $success = file_put_contents($file, $data);

        //rotate
        $fileName = $file;
        $degrees = 270;
        $source = imagecreatefrompng($fileName);
        $rotate = imagerotate($source, $degrees, 0);
        $file_rotate = $upload_dir . mktime() . "9.png";
        imagepng($rotate, $file_rotate);

        //print ini harus di setting
        $output = shell_exec('mspaint /p C:\xampp\htdocs\stikerdepo\assets\print\\'.mktime().'9.png');
        
        //delete file asli
        unlink($file);
        //delete file hasil rotate
        // unlink($file_rotate);

        print $success ? $file : 'Unable to save the file.';
        }

}
