<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class login extends CI_Controller{

    function __construct(){
        parent::__construct();
		$this->load->model('globalmodel');
    }

    public function index(){   
		$this->load->view('view_login');
    }

    public function auth(){ 
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $where = array('username'=>$username,'password'=>$password);
        $check = $this->globalmodel->countWhereData('user',$where);
        if($check > 0){
            $result = $this->globalmodel->getWhereDataArray('user',$where);
            $newdata = array(
                            'username'  => $result[0]['username'],
                            'password'  => $result[0]['password'],
                            'group_id'  => $result[0]['group_id'],
                            'KdCabang'  => $result[0]['KdCabang'],
                            'logged_in' => TRUE
                        );
        
            $this->session->set_userdata($newdata);
            echo json_encode(array('message'=>'success'));
        }else{
            echo json_encode(array('message'=>'not found'));
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('Home');
    }
}