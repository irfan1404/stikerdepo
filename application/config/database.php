<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$active_group = 'default';
$active_record = TRUE;

// $db['default']['hostname'] = '192.168.0.175';
// $db['default']['username'] = 'users3cabang';
// $db['default']['password'] = 'usersukasukses';
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'stikerdepo';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['s3new']['hostname'] = '192.168.0.88';
$db['s3new']['username'] = 'users3';
$db['s3new']['password'] = 'usersukasukses';
$db['s3new']['database'] = 's3new';
$db['s3new']['dbdriver'] = 'mysqli';
$db['s3new']['dbprefix'] = '';
$db['s3new']['pconnect'] = TRUE;
$db['s3new']['db_debug'] = TRUE;
$db['s3new']['cache_on'] = FALSE;
$db['s3new']['cachedir'] = '';
$db['s3new']['char_set'] = 'utf8';
$db['s3new']['dbcollat'] = 'utf8_general_ci';
$db['s3new']['swap_pre'] = '';
$db['s3new']['autoinit'] = TRUE;
$db['s3new']['stricton'] = FALSE;

$db['mobile']['hostname'] = '192.168.0.101';
$db['mobile']['username'] = 'users3cabang';
$db['mobile']['password'] = 'usersukasukses';
$db['mobile']['database'] = 'mobile_salesman';
$db['mobile']['dbdriver'] = 'mysqli';
$db['mobile']['dbprefix'] = '';
$db['mobile']['pconnect'] = TRUE;
$db['mobile']['db_debug'] = TRUE;
$db['mobile']['cache_on'] = FALSE;
$db['mobile']['cachedir'] = '';
$db['mobile']['char_set'] = 'utf8';
$db['mobile']['dbcollat'] = 'utf8_general_ci';
$db['mobile']['swap_pre'] = '';
$db['mobile']['autoinit'] = TRUE;
$db['mobile']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */