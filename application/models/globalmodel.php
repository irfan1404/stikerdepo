<?php
class globalmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }

    function getDataArray($table)
    {
        $query = $this->db->get($table);
        $result = $query->result_array();

        return $result;
    }
    
    function getJoin2TableDataArray($table1,$table2,$id_join1,$id_join2)
    {
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2,$table2.'.'.$id_join2."=".$table1.'.'.$id_join1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function getWhereDataArray($table,$whereArray)
    {
        $query = $this->db->get_where($table,$whereArray);
        $result = $query->result_array();
        return $result;
    }

    function getWhereLikeDataArray($table,$keywordArray,$whereArray=array())
    {
        $this->db->select('*');
        $this->db->from($table);
        if(!empty($whereArray))
        {
            $this->db->where($whereArray);
        }
        $key = array_keys($keywordArray);
        for($i = 0; $i < count($keywordArray); $i++)
        {
            $this->db->like($key[$i],$keywordArray[$key[$i]]);
        }
        $query = $this->db->get(); 
        $result = $query->result_array();
        return $result;
    }

    function getJoin2TableWhereDataArray($table1,$table2,$id_join1,$id_join2,$whereArray)
    {
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2,$table2.'.'.$id_join2."=".$table1.'.'.$id_join1);
        $this->db->where($whereArray);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function insertData($table,$data)
    {
        $query = $this->db->insert($table,$data);
        return $query;
    }

    function countData($table)
    {
        $query = $this->db->get($table);
        $result = $query->num_rows();
        return $result;
    }

    function countWhereData($table,$whereArray)
    {
        $query = $this->db->get_where($table,$whereArray);
        $result = $query->num_rows();
        return $result;
    }

    function deleteData($table,$whereArray)
    {
        $query = $this->db->delete($table,$whereArray); 
        return $query;
    }
    
    function updateData($table,$data,$whereArray)
    {
        $query = $this->db->update($table,$data,$whereArray);
        return $query;
    }

    function get_file_directory($file_id)
    {
        $this->db->select('directory,filename');
        $this->db->from('file');
        $this->db->where('file_id',$file_id);
        $query = $this->db->get();

        $excute = $query->row(); 
        $directory = $excute->directory;
        $file_name = $excute->filename;
        $path = "./assets".$directory.$file_name;
        return $path;
    }

    function getselect($sql){
        $sql 	="SELECT  ".$sql." ";
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
    }

    function getdetailkdoutlet($where){
        $db2 = $this->load->database('mobile',True);
        $db2->select('KdOutlet');
        $db2->select('Nama');
        $db2->from('outlet');
        $db2->where($where);
        $db2->order_by('Nama','ASC');
        $q = $db2->get();   
        return $q->result_array();
    }

    function datakontrol($nokontrol,$cek){
        $this->db->from('kontroldata');
        $this->db->where($nokontrol);
        $q = $this->db->get();
        if($cek=="array"){
            return $q->result_array();
        }elseif($cek=="nums"){
            return $q->num_rows;
        }
    }

    function dataallkontrol($cabang,$cek){
        $this->db->from('kontroldata');
        $this->db->where('KdCabang',$cabang);
        $q = $this->db->get();
        if($cek=="array"){
            return $q->result_array();
        }elseif($cek=="nums"){
            return $q->num_rows;
        }
    }

    function dataall($where,$KdCabang){
        $db3 = $this->load->database('mobile',TRUE);
        $db3->select('`a.KdOutlet')
        ->select('a.Nama')
        ->select('a.Alm1Toko')
        ->select('a.KotaToko')
        ->select('a.TlpToko')
        ->select('b.Latitude')
        ->select('b.Longitude')
        ->select('a.KdCabang')
        ->select('c.NamaCabang') 
        ->from('outlet a')
        ->join('cek_in b','a.KdOutlet = b.KdOutlet','a.KdCabang = b.KdCabang','inner') 
        ->join('cabang c','a.KdCabang = c.KdCabang','inner') 
        ->where($where)
        ->where('a.Nama <>','%testing%')
        ->where('a.KdOutlet <> ','')
        ->where('b.Latitude <>','No Location');
        if($KdCabang =="31"){ $db3->not_like('b.Longitude','-6.167'); }
        if($KdCabang =="32"){ $db3->not_like('b.Longitude','-6.244'); }
        if($KdCabang =="33"){ $db3->not_like('b.Longitude','-6.953'); }
        if($KdCabang =="41"){ $db3->not_like('b.Longitude','-6.167'); }
        if($KdCabang =="42"){ $db3->not_like('b.Longitude','-6.167'); }
        if($KdCabang =="43"){ $db3->not_like('b.Longitude','-6.167'); }
        $db3->group_by('a.KdOutlet');
        $db3->order_by('a.nama','ASC');
        $q = $db3->get();
        return $q->result_array();
    }

    function editData($where,$data,$tabel){
        $this->db->where($where);
        $this->db->set($data);
        $this->db->update($tabel);
    }

    function getdetailarray(){
        $this->db->from('kontroldata');
        $this->db->order_by('nokontrol','DESC');
        $q = $this->db->get();   
        return $q->result_array();
    }

    function instername($table,$data,$whereArray){
        $query = $this->db->update($table,$data,$whereArray);
        return $query;
    }


}