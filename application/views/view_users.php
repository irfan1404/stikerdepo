<div class="z-depth-bottom-1">
    <div class="card-header">
        <h5>Users</h5>
        <br><br>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-grd-info btn-sm" onclick="refresh_data();"><i class="feather icofont icofont-arrow-left"></i>Back</button>
            <?php if($access['create'] == 1){ ?>
                <button class="btn btn-grd-warning btn-sm" id="add-user"><i class="feather icon-plus"></i>Add User</button>
            <?php } ?>
            </div>
        </div>
    </div>
    <form action="<?php echo base_url().$this->uri->segment(1); ?>/save_access" id="form-menu">
    <div class="card-block">
        <div class="sub-title">Group Name: <?php echo $group_name; ?> <input type="hidden" name="group_id" value="<?php echo $group_id; ?>"></div>
        <div class="table-responsive">
            <form action="<?php echo base_url().$this->uri->segment(1); ?>/save_access" id="form-menu">
            <table class="table table-xs table-bordered">
                <thead>
                    <tr>
                        <th width="10">No.</th>
                        <th>Username</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($data) > 0){ ?>
                    <?php $no=1; ?>
                    <?php foreach ($data as $val){ ?>
                        <tr>
                            <td><?php echo $no++."."; ?></td>
                            <td><?php echo $val['username']; ?></td>
                            <td><label style="cursor:pointer;" class="label label-danger" title="delete from group" onclick="delete_from_group('<?php echo $val['username'] ?>')"><i class="icofont icofont-trash"></i></label></td>
                        </tr>
                    <?php } ?>
                    <?php }else{ echo "<tr><td colspan='3'><h6 class='text-center col-md-12 col-xs-12 col-lg-12' > <i class='icofont icofont-emo-worried'></i>   Oops!, Empty data.</h6></td></tr>";} ?>
                </tbody>
            </table>
        </div>
    </div>
    </form>
</div>

<script>
    $("#add-user").click(function(){
        $("#upload2").trigger('click');
        var action = remote_address+"/save_user_group";
        $("#myform2").attr('action',action);
        var obj = "";
        $.ajax({
            url : remote_address+"/users_all",
            success : function(e){
                obj = JSON.parse(e);
                var view ="";
                view += "<div class='col-md-6'>";
                view += "<div class='sub-title'>Add User</div>";
                view += "<input type='hidden' name='group_id' value='<?php echo $group_id; ?>'>";
                view += "<select class='form-control col-sm-12' name='username'>";

                $.each(obj.return,function(key,val){
                    view += "<option value='"+val.username+"'>"+val.username+"</option>";
                });
               
                view += "</select><br>";
                view += "<button type='submit' class='btn btn-sm btn-grd-success' style='margin-left:0px;' id='save'><i class='icofont icofont-save'></i> Save</button>";
                view += "</div>";

                $("#modal-12 .md-content .load-content" ).html(view);
            },
            error : function(e){
                alert('error '+e);
            }
        });
       
        
    });

    function delete_from_group(username){
        swal({
            title: "Are you sure?",
            text: "Delete this user from group,\n"+username,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : remote_address+"/delete_from_group",
                    type: "POST",
                    data : {username:username},
                    success : function(e){
                        var data = JSON.parse(e);
                        if(data.message == 'success'){
                           swal({
                                title: "Success",
                                text: "You clicked the button",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false
                            }, function (isConfirm) {
                                refresh_data();
                            });
                        }else{
                            swal("Error!", "You clicked the button!", "warning");
                        }
                    },error : function(e){
                        swal("Error!\n"+e, "Try again!", "error")
                    }
                });
            } else {
                swal("Cancelled!", "Your file is safe!", "error");
            }
        });
    }

    $("#save").click(function(){
        $("#form-menu").submit();
    });

    $("#form-menu").submit(function(e){
      e.preventDefault();
      var action = $(this).attr('action');
        $.ajax({
            url:action,
            type:"POST",
            data:new FormData(this),
            processData:false,
            contentType:false,
            beforeSend : function(){
                // $("#save").html("Processing..");
                // $("#save").addClass("btn-disabled disabled");
                // $("#save").attr("disabled","disabled"); 
                $("#alert-upload-file").html("");
            },
            success: function(callback){
                var data = JSON.parse(callback);
                if (data.message== 'success'){ 
                    $("#save").html("<i class='icofont icofont-save'></i> Save");
                    $("#save").removeClass("btn-disabled disabled");   
                    $("#save").removeAttr("disabled");  
                    swal({
                        title: "Success",
                        text: "You clicked the button",
                        type: "success",
                        closeOnConfirm: true,
                    }, function (isConfirm) {
                        refresh_data();
                    });
                }else{
                    $("#save").html("<i class='icofont icofont-save'></i> Save");
                    $("#save").removeClass("btn-disabled disabled");
                    $("#save").removeAttr("disabled");  
                    swal("Error!", "Pls Try Again!", "warning");
                }
            },
            error : function(){
                $("#save").html("<i class='icofont icofont-save'></i> Save");
                $("#save").removeClass("btn-disabled disabled");
                $("#save").removeAttr("disabled");  
                swal("Error!", "Pls Try Again!", "warning");
            }
        });
    });

</script>

