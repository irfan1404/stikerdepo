<div class="z-depth-bottom-1">
    <div class="card-header">
        <h5>Otorisasi</h5>
        <br><br>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-grd-info btn-sm" onclick="refresh_data();"><i class="feather icofont icofont-arrow-left"></i>Back</button>
            <?php if($access['update'] == 1){ ?>
                <button class="btn btn-grd-success btn-sm" id="save"><i class="feather icon-save"></i>Save</button>
            <?php } ?>
            </div>
        </div>
    </div>
    <form action="<?php echo base_url().$this->uri->segment(1); ?>/save_access" id="form-menu">
    <div class="card-block">
        <div class="sub-title">Group Name: <?php echo $group_name; ?> <input type="hidden" name="group_id" value="<?php echo $group_id; ?>"></div>
        <div class="table-responsive">
            <form action="<?php echo base_url().$this->uri->segment(1); ?>/save_access" id="form-menu">
            <table class="table table-xs table-bordered">
                <thead>
                    <tr>
                        <th>Menu</th>
                        <th>V</th>
                        <th>R</th>
                        <th>C</th>
                        <th>U</th>
                        <th>D</th>
                        <th>Approve</th>
                        <th>Download</th>
                        <th>print</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php otorisasi_menu($group_id); ?>
                    
                </tbody>
            </table>
        </div>
    </div>
    </form>
</div>

<script>
    $("#add-group").click(function(){
        $("#upload2").trigger('click');
        var view ="";
            view += "<div class='col-md-6'>";
            view += "<div class='sub-title'>Add Group</div>";
            view += "<form action='"+remote_address+"/save' id='myform2'>";
            view += "<input type='text' name='name_group' class='form-control'><br>";
            view += "<button type='submit' class='btn btn-sm btn-grd-success' style='margin-left:0px;' id='save'><i class='icofont icofont-save'></i> Save</button>";
            view += "</form>";
            view += "</div>";
        $("#modal-12 .md-content .load-content" ).html(view);
    });

    function del(id,no){
        swal({
            title: "Are you sure?",
            text: "Delete this group,\n    No: "+no,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : remote_address+"/delete",
                    type: "POST",
                    data : {id:id},
                    success : function(e){
                        var data = JSON.parse(e);
                        if(data.message == 'success'){
                           swal({
                                title: "Success",
                                text: "You clicked the button",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false
                            }, function (isConfirm) {
                                refresh_data();
                            });
                        }else if(data.message == 'error' && data.return == 'file is not exist'){
                            swal("Failed!", "Your file is not exist!", "warning");
                        }else{
                            swal("Error!", "You clicked the button!", "warning");
                        }
                    },error : function(e){
                        swal("Error!\n"+e, "Try again!", "error")
                    }
                });
            } else {
                swal("Cancelled!", "Your file is safe!", "error");
            }
        });
    }

    $("#save").click(function(){
        $("#form-menu").submit();
    });

    $("#form-menu").submit(function(e){
      e.preventDefault();
      var action = $(this).attr('action');
        $.ajax({
            url:action,
            type:"POST",
            data:new FormData(this),
            processData:false,
            contentType:false,
            beforeSend : function(){
                // $("#save").html("Processing..");
                // $("#save").addClass("btn-disabled disabled");
                // $("#save").attr("disabled","disabled"); 
                $("#alert-upload-file").html("");
            },
            success: function(callback){
                var data = JSON.parse(callback);
                if (data.message== 'success'){ 
                    $("#save").html("<i class='icofont icofont-save'></i> Save");
                    $("#save").removeClass("btn-disabled disabled");   
                    $("#save").removeAttr("disabled");  
                    swal({
                        title: "Success",
                        text: "You clicked the button",
                        type: "success",
                        closeOnConfirm: true,
                    }, function (isConfirm) {
                        refresh_data();
                    });
                }else{
                    $("#save").html("<i class='icofont icofont-save'></i> Save");
                    $("#save").removeClass("btn-disabled disabled");
                    $("#save").removeAttr("disabled");  
                    swal("Error!", "Pls Try Again!", "warning");
                }
            },
            error : function(){
                $("#save").html("<i class='icofont icofont-save'></i> Save");
                $("#save").removeClass("btn-disabled disabled");
                $("#save").removeAttr("disabled");  
                swal("Error!", "Pls Try Again!", "warning");
            }
        });
    });

</script>

