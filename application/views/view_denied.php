
<?php $this->load->view('view_header') ?>
<div class="page-body">
     <!-- Page-header start -->
     <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4><?php echo get_name_menu($this->uri->segment(1)); ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Home</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xl-12">
                <div class="alert alert-danger icons-alert" style="padding: 20px 20px;">
                    <button type="button" class="close"  aria-label="Close">
                        <i class="icofont icofont-lock"></i>
                    </button>
                    <p><strong>Access denied!</strong> &nbsp;&nbsp;You don't have authorization to view this page.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('view_footer') ?>
                                