
               <div class="sub-title">Folder</div> 
                <div class="row">
                    <div class="col-md-12">
                    <?php echo breadcrumb($directory); ?>
                    <input type="hidden" class="col-md-12" id="directory" value="<?php echo $directory; ?>" >
                    <input type="hidden" id="counter" value="0">
                        <div class="icon-list-demo" id="btn-back-folder" >
                        <div class="outer-ellipsis"><i class="icofont icofont-arrow-up "  onclick="back_folder('<?php echo $directory; ?>')"></i></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    <?php if($access['create'] == 1){ ?>
                        <button class="btn btn-grd-info btn-sm" id="create-folder"><i class="feather icon-plus"></i>Create Folder</button>
                    <?php } ?>
                    </div>
                </div>
                <br>
                <div class="row folder-list">
                <?php if(count($folder) > 0){ ?>
                    <?php foreach ($folder as $val){ ?>
                    <div class="col-sm-12 col-md-12 col-xl-4 folder-card ">
                        <div class="alert alert-primary ">
                            <div  onclick="refresh_data('<?php echo $val['directory'].$val['filename']."/"; ?>')"><i class="icofont icofont-ui-folder"></i> &nbsp;&nbsp;<?php echo $val['filename']; ?></div>
                            <a href="javascript:void(0);" onclick="delete_folder('<?php echo $val['file_id']; ?>','<?php echo $val['directory'].$val['filename']."/"; ?>')" title="delete"  class="del-icon" data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="" data-original-title="download"><i class="feather icon-trash"></i></a>
                        </div>
                    </div>
                    <?php } ?>
                    <?php }else{ echo "<h6 class='text-center col-md-12 col-xs-12 col-lg-12' > <i class='icofont icofont-emo-worried'></i>   Oops!, Empty folder.</h6>";} ?>
                </div>
                <br><br><br>
                <!-- Border Alert end -->
                <!-- Left Border Alert start -->
                <div class="sub-title">File</div>
                <div class="row">
                    <div class="col-md-12">
                    <?php if($access['create'] == 1){ ?>
                        <button class="btn btn-grd-warning btn-sm" id="upload" ><i class="feather icon-upload"></i>Upload</button>
                    <?php } ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <?php if(count($data) > 0){ ?>
                    <?php foreach ($data as $val){ ?>
                    <div class="col-xl-4 col-md-6 " >
                        <div class="card social-card bg-simple-c-green">
                            <div class="card-block">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <div style="cursor: pointer;"  
                                        <?php if($access['update'] == 1){ ?>
                                        data-toggle="popover" data-placement="right" data-html="true" 
                                        title="
                                        <input type='hidden' id='file_id_<?php echo $val['file_id']; ?>' value='<?php echo $val['file_id']; ?>'>
                                        <input type='text' id='subject_<?php echo $val['file_id']; ?>' class='form-control form-control-sm' placeholder='Edit subject'><br>
                                        <input type='text' class='form-control form-control-sm' placeholder='Edit tag' id='tag_<?php echo $val['file_id']; ?>'><br>
                                        <button class='btn btn-primary btn-sm btn-grd-info btn-block' id='btn-save-file_<?php echo $val['file_id']; ?>' onclick='file_edit(<?php echo $val['file_id']; ?>)'><i class='icofont icofont-save'></i>save</button>
                                        <div id='alert_file_<?php echo $val['file_id']; ?>'></div>
                                        "
                                        <?php } ?>
                                        >
                                            <h6  class="m-b-1" id='v_subject_<?php echo $val['file_id']; ?>'><?php echo $val['subject'] ?></h6>
                                            <p id='v_tag_<?php echo $val['file_id']; ?>'># <?php echo $val['tag'];?></p>
                                        </div>
                                        <p><?php echo $val['filename'];?></p>
                                        <a href="javascript:void(0);" class="mytooltip tooltip-effect-7 m-b-0 "><?php echo date("d/m/Y H:i",strtotime($val['tanggal'])); ?><span class="tooltip-content2"><div class="font-10"><?php echo "file_id: ". $val['file_id']."<br>".$val['user_add']; ?></div></span></a>
                                    </div>
                                </div>
                            </div>
                            <?php $url = base_url()."assets".$val['directory'].$val['filename']; ?> 
                            <a href="javascript:void(0);"  class="download-icon" >
                                <?php if($access['update'] == 1){ ?>
                                    <i class="icofont icofont-bubble-right" onclick="move_file(<?php echo $val['file_id']; ?>);" data-toggle="tooltip" data-trigger="hover" data-placement="right"  data-original-title="move"></i>
                                <?php }else{ ?>
                                    <i class="feather icon-lock" data-toggle="tooltip" data-trigger="hover" data-placement="right"  data-original-title="move is locked"></i>
                                <?php } ?>
                                <br>
                                <?php if($access['download'] == 1){ ?>
                                <i class="feather icon-arrow-down"  onclick="download_('<?php echo $url; ?>');" data-toggle="tooltip" data-trigger="hover" data-placement="right"  data-original-title="download"></i>
                                <?php }else{ ?>
                                    <i class="feather icon-lock"  data-toggle="tooltip" data-trigger="hover" data-placement="right"  data-original-title="download is locked"></i>
                                <?php } ?>
                            </a>

                            <?php if($access['delete'] == 1){ ?>
                            <a href="javascript:void(0);" class="delete-icon" >
                                <i class="feather icon-trash"  onclick="del('<?php echo $val['file_id']; ?>','<?php echo $val['filename']; ?>')"  data-toggle="tooltip" data-trigger="hover" data-placement="left" title="" data-original-title="delete"></i>
                            </a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php }else{ echo "<h6 class='text-center col-md-12 col-xs-12 col-lg-12' > <i class='icofont icofont-emo-worried'></i>   Oops!, Empty file.</h6>";} ?>
                    <!-- social download  end -->
                </div>
                <!-- Left Border Alert start -->
                  
                
<div class="row">

</div>

 
<script>
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function() {
        $('[data-toggle="popover"]').popover({
            html: true,
            content: function() {
                return $('#primary-popover-content').html();
            }
        });
    });
    
    $("#upload").click(function(){ 
        var directory = $("#directory").val();
        $("#small-modal").modal("show");
        $("#form-directory").val(directory);
        var action = remote_address+"/save";
        $("#myform").attr('action',action);
    });


    $("#create-folder").click(function(){
        var i =  parseInt($("#counter").val()) + 1;
        var folder = "<div class='col-sm-12 col-md-12 col-xl-4' style='margin-bottom:-26px;' id='new_"+i+"'>";
            folder += "<div class='alert alert-warning'>";
            folder += "<button type='button' class='close' onclick='remov(\""+i+"\");'>";
            folder += "<i class='icofont icofont-close-line-circled'></i>";
            folder += "</button>";
            folder += "<i class='icofont icofont-ui-folder'></i> &nbsp;&nbsp;";
            folder += "<input type='text' id='new_folder_"+i+"' onkeyup='create_folder(event,\""+i+"\")' value='New folder' style='border:none;color:rgb(254, 147, 101);'>";
            folder += "</div>";
            folder += "</div>";
        
        $(".folder-list:first").append(folder);
        $("#counter").val(i);
        Setfocus(i);
    });

    function remov(id){
        $("#new_"+id).remove();
    };

    function Setfocus(id) {
        var elem = document.getElementById("new_folder_"+id);
        var elemLen = elem.value.length;
        // For IE Only
        if (document.selection) {
            // Set focus
            elem.focus();
            // Use IE Ranges
            var oSel = document.selection.createRange();
            // Reset position to 0 & then set at end
            oSel.moveStart('character', -elemLen);
            oSel.moveStart('character', elemLen);
            oSel.moveEnd('character', 0);
            oSel.select();
        }
        else if (elem.selectionStart || elem.selectionStart == '0') {
            // Firefox/Chrome
            elem.selectionStart = elemLen;
            elem.selectionEnd = elemLen;
            elem.focus();
        } // if
    } 
    function download_(url){
        window.open(url, '_blank');
    }

    function del(file_id){
        swal({
            title: "Are you sure?",
            text: "Delete this file,\n    File ID: "+file_id,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : remote_address+"/delete",
                    type: "POST",
                    data : {file_id:file_id},
                    success : function(e){
                        var data = JSON.parse(e);
                        if(data.message == 'success'){
                           swal({
                                title: "Success",
                                text: "You clicked the button",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false
                            }, function (isConfirm) {
                                refresh_data();
                            });
                        }else if(data.message == 'error' && data.return == 'file is not exist'){
                            swal("Failed!", "Your file is not exist!", "warning");
                        }else{
                            swal("Error!", "You clicked the button!", "warning");
                        }
                    },error : function(e){
                        swal("Error!\n"+e, "Try again!", "error")
                    }
                });
            } else {
                swal("Cancelled!", "Your file is safe!", "error");
            }
        });
    }

    function create_folder(e,id){
        if (window.event) {
            var code = e.keyCode;
        }else if (e.which) {
            var code = e.which;
        }

        var folder_name = $("#new_folder_"+id).val();
        var directory = $("#directory").val();
        
        if(code == 13){
            $.ajax({
                url : remote_address+"/create_folder",
                type: "POST",
                data : {folder:folder_name,directory:directory},
                success : function(e){
                    var data = JSON.parse(e);
                    if(data.message == 'success'){
                        refresh_data(directory);
                    }else{
                        $("#new_folder_"+id).val("Sorry, folder name already is exist."); 
                    }
                }
            });
        }
    }

    function delete_folder(file_id,dir){
        swal({
            title: "Are you sure?",
            text: "Delete this folder,\n Will Lost your data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : remote_address+"/delete_folder",
                    type: "POST",
                    data : {file_id:file_id,directory:dir},
                    success : function(e){
                        var data = JSON.parse(e);
                        if(data.message == 'success'){
                            swal({
                                title: "Success",
                                text: "You clicked the button",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false
                            }, function (isConfirm) {
                                refresh_data();
                            });
                        }else if(data.message == 'error' && data.return == 'file is not exist'){
                            swal("Failed!", "Your file is not exist!", "warning");
                        }else{
                            swal("Error!", "You clicked the button!", "warning");
                        }
                    },error : function(e){
                        swal("Error!\n"+e, "Try again!", "error")
                    }
                });
            } else {
                swal("Cancelled!", "Your file is safe!", "error");
            }
        });
    }
    function file_edit(id){
        var file_id = $("#file_id_"+id).val();
        var subject = $("#subject_"+id).val();
        var tag = $("#tag_"+id).val();

        $.ajax({
            url : remote_address+"/update",
            type : "POST",
            data : {file_id:file_id,subject:subject,tag:tag},
            beforeSend : function(){
                $("#alert_file_"+id).html('');
                $("#btn-save-file_"+id).html('........');
                $("#btn-save-file_"+id).attr('disabled');
            },success : function(e){
                $("#btn-save-file_"+id).html('<i class="icofont icofont-save"></i> Save');
                $("#btn-save-file_"+id).removeAttr('disabled');
                if(subject !=""){
                    $("#v_subject_"+id).html(subject);
                }
                if(tag !=""){
                    $("#v_tag_"+id).html("# "+tag);
                }
            },error : function(){
                $("#btn-save-file_"+id).html('<i class="icofont icofont-save"></i> Save');
                $("#btn-save-file_"+id).removeAttr('disabled');
            }
        });
    }

    function back_folder(){
        var directory = $("#directory").val();
        var dir = "";
        var res = directory.split("/");
        for(i = 0 ; i < res.length -2 ; i++ ){
            dir += res[i]+"/";
        }

        refresh_data(dir);
    }

    function hide_show_button_back_folder(){
        var directory = $("#directory").val();
        var dir_menu = "<?php if($this->uri->segment(1) != ""){echo get_directory_menu($this->uri->segment(1));}else{echo get_directory_menu("dashboard");} ?>";
        if(directory != dir_menu){
            $("#btn-back-folder").show();
        }else{
            $("#btn-back-folder").hide();
        }
    }
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        hide_show_button_back_folder();
    });

    $(document).ready(function() {
        $('[data-toggle="popover"]').popover({
            html: true,
            content: function() {
                return $('#primary-popover-content').html();
            }
        });
    });
</script>