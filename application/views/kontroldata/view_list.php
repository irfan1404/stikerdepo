
<button type="button" class="btn btn-success btn-outline-success waves-effect md-trigger" id="add-outlet" >Add Data</button>
<!-- <button type="button" class="btn btn-warning btn-outline-warning" id="printall" onclick="printall()">Print Canvas Outlet</button>
<button type="button" class="btn btn-info btn-outline-info"><a href="<?php echo base_url();?>kontroldata\printallpdf">Print PDF Outlet</a></button> -->
<br>
<br>
<div class="page-body navbar-page">

    <form action="">
    <div class="card-block table-border-style">
        <div class="table-responsive"> 
            <table class="table table-hover mytables" id="tabel_list" name="tabel_list">
                <thead>
                    <tr>
                    <th>No</th>
                    <th>Nama Depo</th>
                    <th>Alamat</th>
                    <th>Kota Toko</th>
                    <th>Kode Outlet</th>
                    <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($list)){?>
                        <? foreach($list as $lst ){ ?>
                        <tr>
                            <td align="left"><?=$lst['nokontrol']?></td>
                            <td align="left"><?=$lst['Nama']?></td>
                            <td align="left"><?=$lst['AltToko']?></td>
                            <td align="left"><?=$lst['KotaToko']?></td>
                            <td align="left"><?=$lst['KdOutlet']?></td>
                            <?if($access['update']=="1"){?>
                                <td><label style="cursor:pointer;" class="label label-primary"  title="Edit" onclick="edit('<?=$lst['nokontrol']?>','<?=$lst['Nama']?>','<?=$lst['AltToko']?>','<?=$lst['KotaToko']?>','<?=$lst['KdOutlet']?>')"><i class="icofont icofont-ui-edit"></i></label>
                            <? }?>
                            <?if($access['delete']=="1"){?>
                                <label style="cursor:pointer;" class="label label-danger" title="Delete" onclick="del('<?=$lst['nokontrol']?>')"><i class="icofont icofont-trash"></i></label>
                            <?}?>

                            <?
                            if($access['print']=="1"){?>
                                <label style="cursor:pointer;" class="label label-success myDIv" title="View QR Card" onclick="print('<?=$lst['nokontrol']?>')"><i class="icofont icofont-eye"></i></label>
                            <?}?>

                            <?
                            if($access['print']=="1"){?>
                                <!-- <a href="<?php echo base_url();?>kontroldata\printpdf\<?=$lst['nokontrol']?>"><i class="icofont icofont-print"></i></a> -->
                                <!-- <label style="cursor:pointer;" class="label label-info" title="print PDF" onclick="printpdf()"><i class="icofont icofont-print"></i></label> -->
                                <label style="cursor:pointer;" class="label label-warning myDIv" title="print to TSC" onclick="privew_and_print('<?=$lst['KdOutlet']?>','<?=$lst['Nama']?>','<?=$lst['AltToko']?>','<?php echo $lst['nama_gambar'];?>')"><i class="icofont icofont-print"></i></label>
                            <?}?>
                            </td>
                            
                        </tr>
                        <? } ?>
                    <? }else{
                        echo "<tr><td colspan='10' align='center'>Tidak Ada Data</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
            </div>
        </div>
    </form>
</div>

<!-- disini akan jadi areal sticker -->
<div style="display:none">
<img id="background"  src="<?php echo base_url().'assets/upload/bg.png'?>" width="1200px" height="1500px" style="display:none">
<img id="qrcode1"  src="<?php echo base_url().'assets/upload/big.jpg';?>" width="1200px" height="1500px" style="display:none">
<img id="qrcode2"  src="<?php echo base_url().'assets/upload/big.jpg';?>" width="1200px" height="1500px" style="display:none">
<canvas id="myCanvas" width="1200" height="1500" style="border-style: none;display: none;"></canvas>
</div>

<form method="post" accept-charset="utf-8" name="form1">
	<input name="hidden_data" id='hidden_data' type="hidden"/>
</form>

<script type="text/javascript">

$('.mytables').DataTable({
    searching:true,
    ordering:true,
    lengtMenu:[[5,10,25,50,-1],[5,10,25,50,"all"]],
    creatRow:function(row, data ,index){
        if(data[5].replace(/[\$,]/g, '')*1>150000){
            $('td',row).eq(5).addClass('text-success');
        }
    }
});

    $("#add-outlet").click(function(){
        $("#modalcek").trigger('click');
        var obj = "";
        $.ajax({
            url : remote_address+"/outlet_all",
            success : function(e){
                obj = JSON.parse(e);
                var view ="";
                view+="<div class='modal-header'>";
                view+="<h4 class='modal-title'>Tambah Data</h4>";
                view+="</div>";
                view+="<div class='modal-body'>";
                view += "<input type='hidden' name='action' value='simpan' class='form-control'><br>";
                view+="<label >Kodeoutlet</label>";
                view+="<div class='col-sm-12'>";
                view+="<select name ='kodeoutlet' class='form-control form-control-capitalize'>";
                $.each(obj.return,function(key,val){
                    view += "<option value='"+val.KdOutlet+"'>"+val.Nama+"</option>";
                });
                view+="</select><br>";
                view+="</div>";
                $("#modal-3 .md-content .load-content" ).html(view);
            },
            error : function(e){
                alert('error '+e);
            }
        });
       
    });

    

function del(id){
        swal({
            title: "Are you sure?",
            text: "Delete this group,\n    No: "+id,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : remote_address+"/delete",
                    type: "POST",
                    data : {id:id},
                    success : function(e){
                        var data = JSON.parse(e);
                        if(data.message == 'success'){
                           swal({
                                title: "Success",
                                text: "You clicked the button",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false
                            }, function (isConfirm) {
                                location.reload();
                            });
                        }else if(data.message == 'error' && data.return == 'file is not exist'){
                            swal("Failed!", "Your file is not exist!", "warning");
                        }else{
                            swal("Error!", "You clicked the button!", "warning");
                        }
                    },error : function(e){
                        swal("Error!\n"+e, "Try again!", "error")
                    }
                });
            } else {
                swal("Cancelled!", "Your file is safe!", "error");
            }
        });
    }


    function edit(id,Nama,Alamat,Kota){
        $("#modalcek").trigger('click');
        var view ="";
        view+="<div class='modal-header'>";
        view+="<h4 class='modal-title'>Edit Data</h4>";
        view+="</div>";
        view+="<div class='modal-body'>";
        view += "<input type='hidden' name='id' value='"+id+"' class='form-control'><br>";
        view += "<input type='hidden' name='action' value='edit' class='form-control'><br>";
        view+="<label >Nama</label>";
        view += "<input type='text' name='Nama' value='"+Nama+"' class='form-control'><br>";
        view+="<label >Alamat</label>";
        view += "<input type='text' name='Alamat' value='"+Alamat+"' class='form-control'><br>";
        view+="<label >Kota</label>";
        view += "<input type='text' name='Kota' value='"+Kota+"' class='form-control'><br>";
        view+="</div>";
        $("#modal-3 .md-content .load-content" ).html(view);
    }

    function print(id){
        $.ajax({
            url : remote_address+"/printadd",
            type : "POST",
            data :{id:id},
            beforeSend : function(){
                loader_time();
            },
            success :function(html){
                $("#load-time2").html(html).fadeIn('slow');
            },error : function(res){
                swal("eror data", "error")
            }
        });
    }

    function privew_and_print(kdoutlet,nama,alamat,qrcode){

        document.getElementById('myCanvas').style.display="";
        $("#qrcode1").attr("src",base_url+"/assets/upload/"+qrcode);
        $("#qrcode2").attr("src",base_url+"/assets/upload/"+qrcode);

        setTimeout(function(){ 
            
       // Mensetting Variabel
        var img1            = document.getElementById('background');
        var img2            = document.getElementById('qrcode1');
        var img3            = document.getElementById('qrcode2');
        console.log('iku : ',img2);
        console.log('iki : ',img3);
        var AltToko         = alamat;
        var Nama            = nama;
        var KdOutlet        = kdoutlet;
        var canvas          = document.getElementById("myCanvas");
        var context         = canvas.getContext("2d");
        var width           = img2.width;
        var height          = img2.height;
        canvas.width        = width;
        canvas.height       = height;
        context.font='40px arial';

        // background
        context.drawImage(img1, 0, 0, width, height);
        var image1 = context.getImageData(0, 0, width, height);
        var imageData1 = image1.data;

        //gambar1
        context.drawImage(img2, 0, 250, 255, 255);
        var image2 = context.getImageData(30, 150, 100, 100);
        var imageData2 = image2.data;
        context.fillText(KdOutlet,12,535,500,100);
        context.fillText(Nama,12,580,500,100);
        context.fillText(AltToko,12,625,500,100);

        //gambar2
        context.drawImage(img3, 0, 1122, 255, 255);
        var image3 = context.getImageData(30, 150, 100, 100);
        var imageData3 = image3.data;
        context.fillText(KdOutlet,12,1408,500,100);
        context.fillText(Nama,12,1453,500,100);
        context.fillText(AltToko,12,1498,500,100);


        setTimeout(function(){ 
            var r=confirm("Apakah Anda Ingin Mencetak No Dokumen "+qrcode+" ?")
            if (r==true)
            {
                PrintCanvas();
            }
            else
            {
                return false;
            }
        },1000);

        }, 1000);
        
    }

    function PrintCanvas(){
                var canvas = document.getElementById("myCanvas");
				var dataURL = canvas.toDataURL("image/png");
				document.getElementById('hidden_data').value = dataURL;
				var fd = new FormData(document.forms["form1"]);

				var xhr = new XMLHttpRequest();
				xhr.open('POST', 'http://localhost:8080/stikerdepo/home/cetak/', true);

				xhr.upload.onprogress = function(e) {
					if (e.lengthComputable) {
						var percentComplete = (e.loaded / e.total) * 100;
						console.log(percentComplete + '% uploaded');
						alert('Succesfully Printed');
					}
				};

				xhr.onload = function() {

				};
				xhr.send(fd);
    }


    function printall(){
        $.ajax({
            url:remote_address+"/printalloutlet",
            type:"POST",
            beforesend : function (){
                loader_time();
            },success : function(html){
                $("#load-time2").html(html).fadeIn('slow');
            },error :function(res){
                swal("eror data", "error")
            }
        });
    }

    // function printallpdf(){
    //     $.ajax({
    //         url:remote_address+"/printallpdf"
    //     });
    // }

    function printpdf(){
        $.ajax({
            url:remote_address+"/printpdf"
        });
    }


    
</script>
