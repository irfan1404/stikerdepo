<?php $this->load->view('view_header') ?>
<div class="page-body">
     <!-- Page-header start -->
     <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <div class="d-inline">
                        <h4><?php echo get_name_menu($this->uri->segment(1)); ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index-1.htm"> <i class="feather icon-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Home</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div id="load-time2">
                    <div class='row'>
                        <div class='col-xl-12 col-md-12 col-xs-12'>
                            <div class='loader-block' style='height: 430px;'>
                                    <div class="loader animation-start">
                                    <span class="circle delay-1 size-2"></span>
                                    <span class="circle delay-2 size-4"></span>
                                    <span class="circle delay-3 size-6"></span>
                                    <span class="circle delay-4 size-7"></span>
                                    <span class="circle delay-5 size-7"></span>
                                    <span class="circle delay-6 size-6"></span>
                                    <span class="circle delay-7 size-4"></span>
                                    <span class="circle delay-8 size-2"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-info btn-outline-info waves-effect md-trigger" id="modalcek" style="border:none;" data-modal="modal-3"></button>
            </div>
        </div>
    </div>
</div>
        
    </div>
</div>
<?php $this->load->view('view_footer') ?>

                                