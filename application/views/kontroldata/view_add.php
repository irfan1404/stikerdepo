<?php $this->load->view('view_header') ?>
<h4><?=$title?></h4>
<br>
<div class="page-body navbar-page">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <form action="">
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <table class="table table-hover" id="tabel_list" name="tabel_list">
                                <thead>
                                
                                </thead>
                                <tbody>
                                 
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
</script>
<?php $this->load->view('view_footer') ?>