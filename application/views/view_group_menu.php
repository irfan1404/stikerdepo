
<div class="card-header">
    <h5>Group Menu</h5>
    <br><br>
    <div class="row">
        <div class="col-md-12">
        <?php if($access['create'] == 1){ ?>
            <button class="btn btn-grd-info btn-sm" id="add-group"><i class="feather icon-plus"></i>Add Group</button>
        <?php } ?>
        </div>
    </div>
</div>
<div class="card-block">
    <div class="table-responsive">
        <table class="table table-xs table-bordered">
            <thead>
                <tr>
                    <th width="10">No.</th>
                    <th>Group Menu</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                foreach($data as $val){ 
                ?>
                <tr>
                    <th scope="row"><?php echo $no."."; ?></th>
                    <td><?php echo $val['group_name']; ?></td>
                    <td>
                        <label style="cursor:pointer;" class="label label-primary" title="edit" onclick="edit('<?php echo $val['group_id'];?>','<?php echo $val['group_name']; ?>')"><i class="icofont icofont-ui-edit"></i></label>
                        <label style="cursor:pointer;" class="label label-danger" title="delete" onclick="del('<?php echo $val['group_id'] ?>','<?php echo $no; ?>')"><i class="icofont icofont-trash"></i></label>
                        <label style="cursor:pointer;" class="label label-inverse" title="otorisasi" onclick="otorisasi('<?php echo $val['group_id'] ?>','<?php echo $val['group_name']; ?>')"><i class="icofont icofont-key"></i></label>
                        <label style="cursor:pointer;" class="label label-warning" title="users" onclick="users('<?php echo $val['group_id'] ?>','<?php echo $val['group_name']; ?>')"><i class="icofont icofont-users"></i></label>
                    </td>
                </tr>
                <?php $no++;} ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $("#add-group").click(function(){
        $("#upload2").trigger('click');
        var view ="";
            view += "<div class='col-md-6'>";
            view += "<div class='sub-title'>Add Group</div>";
            view += "<form action='"+remote_address+"/save' id='myform2'>";
            view += "<input type='text' name='name_group' class='form-control'><br>";
            view += "<button type='submit' class='btn btn-sm btn-grd-success' style='margin-left:0px;' id='save'><i class='icofont icofont-save'></i> Save</button>";
            view += "</form>";
            view += "</div>";
        $("#modal-12 .md-content .load-content" ).html(view);
    });

    function del(id,no){
        swal({
            title: "Are you sure?",
            text: "Delete this group,\n    No: "+no,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url : remote_address+"/delete",
                    type: "POST",
                    data : {id:id},
                    success : function(e){
                        var data = JSON.parse(e);
                        if(data.message == 'success'){
                           swal({
                                title: "Success",
                                text: "You clicked the button",
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false
                            }, function (isConfirm) {
                                refresh_data();
                            });
                        }else if(data.message == 'error' && data.return == 'file is not exist'){
                            swal("Failed!", "Your file is not exist!", "warning");
                        }else{
                            swal("Error!", "You clicked the button!", "warning");
                        }
                    },error : function(e){
                        swal("Error!\n"+e, "Try again!", "error")
                    }
                });
            } else {
                swal("Cancelled!", "Your file is safe!", "error");
            }
        });
    }

    function edit(id,name){
        $("#upload2").trigger('click');
        var view ="";
            view += "<div class='col-md-6'>";
            view += "<div class='sub-title'>Edit Group</div>";
            view += "<form action='"+remote_address+"/save' id='myform2'>";
            view += "<input type='hidden' name='id' value='"+id+"' class='form-control'><br>";
            view += "<input type='text' name='name_group' value='"+name+"' class='form-control'><br>";
            view += "<button type='submit' class='btn btn-sm btn-grd-success' style='margin-left:0px;' id='save'><i class='icofont icofont-save'></i> Save</button>";
            view += "</form>";
            view += "</div>";
        $("#modal-12 .md-content .load-content" ).html(view);
    }

    
    function otorisasi(group_id,group_name){
        $.ajax({
            url : remote_address+"/access",
            type : "POST",
            data : {group_id:group_id,group_name:group_name},
            beforeSend : function(){
                var loader = '<div class="text-center loader-block"  style="height: 430px;"><div class="preloader4"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>';
                $("#load-data").html(loader);
            },
            success : function(html){
                $("#load-data").html(html).fadeIn('slow');;
            },
            error : function(e){
                alert("error load data "+e);
            }

        });
    }

    function users(group_id,group_name){
        $.ajax({
            url : remote_address+"/users",
            type : "POST",
            data : {group_id:group_id,group_name:group_name},
            beforeSend : function(){
                var loader = '<div class="text-center loader-block"  style="height: 430px;"><div class="preloader4"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>';
                $("#load-data").html(loader);
            },
            success : function(html){
                $("#load-data").html(html).fadeIn('slow');;
            },
            error : function(e){
                alert("error load data "+e);
            }

        });
    }

    
</script>

