<!DOCTYPE html>
<html lang="en">
<head>
    <title>Stiker Depo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <link rel="icon" href="<?php echo base_url();?>assets\adminty\files\assets\images\globe.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\bootstrap\css\bootstrap.min.css">
    <link href="<?php echo base_url();?>assets\adminty\files\assets\pages\jquery.filer\css\jquery.filer.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets\adminty\files\assets\pages\jquery.filer\css\themes\jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\icon\feather\css\feather.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\icon\themify-icons\themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\icon\icofont\css\icofont.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\css\style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\css\jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\css\component.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\jstree\css\style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\pages\treeview\treeview.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\dropzone.js\css\dropzone.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\dropzone.js\css\custom-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\sweetalert\css\sweetalert.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets\adminty\files\bower_components\select2\css\select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\bootstrap-multiselect\css\bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\multiselect\css\multi-select.css">
    <!-- light-box css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\ekko-lightbox\css\ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\lightbox2\css\lightbox.css">
        <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\pages\data-table\css\buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets\adminty\files\assets\css\print.css">
</head>
<script>
    var base_url = "<?php echo base_url(); ?>";
    var uri_segment = "<?php if($this->uri->segment(1) != ''){echo $this->uri->segment(1);}else{echo "home";} ?>";
    var remote_address = base_url+uri_segment;
</script>

<body>
    <!-- Pre-loader start -->
        <div class="theme-loader">
            <div class="ball-scale">
                <div class='contain'>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                </div>
            </div>
        </div>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="index-1.htm">
                        <img class="img-fluid" style="height:60px" src="<?php echo base_url();?>assets\adminty\files\assets\images\logovcibaru.png" alt="Theme-Logo">
                        VCI
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">

                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                       <span><?php echo $this->session->userdata('username'); ?></span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="<?php echo base_url(); ?>login/logout">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>  

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                        <?php echo menu_parents(); ?>
                            
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- content -->