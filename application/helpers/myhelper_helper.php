<?php

function menu_parents()
{
    $ci =& get_instance();
    $group_id = $ci->session->userdata('group_id');
    $active_menu = $ci->uri->segment(1);
    $ci->db->select('*');
    $ci->db->from('menu_access');
    $ci->db->join('menu','menu.menu_id = menu_access.menu_id');
    $ci->db->where('menu.parent','0');
    $ci->db->where('menu_access.group_id',$group_id);
    $ci->db->where('menu_access.view','1');
    $ci->db->where('menu.status','1');
    $ci->db->order_by('menu.order','ASC');
    $query = $ci->db->get();
    $result = $query->result_array();

    foreach ($result as $val_parent)
    {
        echo "<div class='pcoded-navigatio-lavel'>".$val_parent['menu_alias']."</div>";
        $parent_id = $val_parent['menu_id'];
        $first_childs = menu_childs($parent_id);
        foreach ($first_childs as $val_first_child )
        {
            $have_childs    = have_childs($val_first_child['menu_id']);
            $hasmenu        = ($have_childs == 1) ? "pcoded-hasmenu":"";
            $active_parent_menu = active_parent_menu($val_first_child['menu_id']);
            $active_trigger = ($active_parent_menu == 1)? "pcoded-trigger":"";
            echo "<ul class='pcoded-item pcoded-left-item'>
                    <li class='".$hasmenu." ".$active_trigger."'>
                        <a href='".$val_first_child['url']."'>
                            <span class='pcoded-micon'><i class='feather ".$val_first_child['icon']."'></i></span>
                            <span class='pcoded-mtext'>".$val_first_child['menu_alias']."</span>
                        </a>";
                    if($have_childs == 1){
                        echo " <ul class='pcoded-submenu'>";
                        $second_childs = menu_childs($val_first_child['menu_id']); 
                        foreach ($second_childs as $val_second_child)
                        {
                            $have_third_childs    = have_childs($val_second_child['menu_id']);
                            $hasmenu_third        = ($have_third_childs == 1) ? "pcoded-hasmenu":"";
                            $active  = ($active_menu == $val_second_child['url'])? "active":"";
                            $active_parent_second_menu = active_parent_menu($val_second_child['menu_id']);
                            $active_second_trigger = ($active_parent_second_menu == 1)? "pcoded-trigger":"";
                            echo "<li class='".$active." ".$hasmenu_third." ".$active_second_trigger."'>
                                    <a href='".$val_second_child['url']."'>
                                        <span class='pcoded-mtext'>".$val_second_child['menu_alias']."</span>
                                    </a>";
                                if($have_third_childs == 1)
                                {
                                    $third_childs = menu_childs($val_second_child['menu_id']); 
                                    echo "<ul class='pcoded-submenu'>";
                                    foreach ($third_childs as $val_third_child)
                                    {
                                        $active  = ($active_menu == $val_third_child['url'])? "active":"";
                                        echo "<li class='".$active."'>
                                                <a href='".$val_third_child['url']."'>
                                                    <span class='pcoded-mtext'>".$val_third_child['menu_alias']."</span>
                                                </a>
                                              </li>";
                                    }
                                    echo " </ul>";
                                }

                            echo "</li>";
                        }
                        echo " </ul>";
                    }
                       
            echo "  </li>
                 </ul>";
            
        }
    }
}

function directory_parent()
{
    $ci =& get_instance();
    
    $group_id = $ci->session->userdata('group_id');
    $active_menu = $ci->uri->segment(1);
    $ci->db->select('*');
    $ci->db->from('menu_access');
    $ci->db->join('menu','menu.menu_id = menu_access.menu_id');
    $ci->db->where('menu.parent','0');
    $ci->db->where('menu_access.group_id',$group_id);
    $ci->db->where('menu_access.view','1');
    $ci->db->where('menu.status','1');
    $ci->db->order_by('menu.order','ASC');
    $query = $ci->db->get();
    $result = $query->result_array();
    echo "<ul>";
    foreach ($result as $val_parent)
    {
        echo "<li data-jstree='{\"opened\":true}'>".$val_parent['menu_alias'];
            $parent_id = $val_parent['menu_id'];
            $first_childs = directory_childs($parent_id);
            echo "<ul>";
                foreach ($first_childs as $val_first_child )
                {
                    $click1 = ($val_first_child['url'] != "javascript:void(0);") ? 'onclick="get_directory(\''.$val_first_child['directory'].'\',\''.$val_first_child['url'].'\')"' : "";
                    echo "<li data-jstree='{\"opened\":true}' ".$click1.">".$val_first_child['menu_alias'];
                        $second_childs = menu_childs($val_first_child['menu_id']); 
                        echo "<ul>";
                            foreach ($second_childs as $val_second_child)
                            {
                                $click2 = ($val_second_child['url'] != "javascript:void(0);") ? 'onclick="get_directory(\''.$val_second_child['directory'].'\',\''.$val_second_child['url'].'\')"' : "";
                                echo "<li data-jstree='{\"opened\":true}' ".$click2.">".$val_second_child['menu_alias'];
                                    $third_childs = menu_childs($val_second_child['menu_id']); 
                                    echo "<ul>";
                                        foreach ($third_childs as $val_third_child)
                                        {
                                            $click3 = ($val_third_child['url'] != "javascript:void(0);") ? 'onclick="get_directory(\''.$val_third_child['directory'].'\',\''.$val_third_child['url'].'\')"' : "";
                                            echo "<li data-jstree='{\"opened\":true}' ".$click3.">".$val_third_child['menu_alias'];

                                            echo "</li>";
                                        }
                                    echo "</ul>";
                                echo "</li>";
                            }
                        echo "</ul>";
                    echo "</li>";
                }
            echo "</ul>";
        echo "</li>";
    }
    echo "</ul>";
}

function directory_childs($parent_id)
{
    $ci =& get_instance();
    $group_id = $ci->session->userdata('group_id');

    $ci->db->select('*');
    $ci->db->from('menu_access');
    $ci->db->join('menu','menu.menu_id = menu_access.menu_id');
    $ci->db->where('menu.parent',$parent_id);
    $ci->db->where('menu_access.group_id',$group_id);
    $ci->db->where('menu_access.view','1');
    $ci->db->where('menu.status','1');
    $ci->db->order_by('menu.order','ASC');
    $query = $ci->db->get();
    $result = $query->result_array();

    return $result;
}

function menu_childs($parent_id)
{
    $ci =& get_instance();
    $group_id = $ci->session->userdata('group_id');

    $ci->db->select('*');
    $ci->db->from('menu_access');
    $ci->db->join('menu','menu.menu_id = menu_access.menu_id');
    $ci->db->where('menu.parent',$parent_id);
    $ci->db->where('menu_access.group_id',$group_id);
    $ci->db->where('menu_access.view','1');
    $ci->db->where('menu.status','1');
    $ci->db->order_by('menu.order','ASC');
    $query = $ci->db->get();
    $result = $query->result_array();

    return $result;
}

function have_childs($menu_id)
{
    $ci =& get_instance();

    $query = $ci->db->get_where('menu',array('parent'=>$menu_id));
    if($query->num_rows() > 0){
        return 1;
    }else{
        return 0;
    }
}

function active_parent_menu($menu_id)
{
    $ci =& get_instance();
    $active_url = $ci->uri->segment(1);
    $query = $ci->db->get_where('menu',array('url'=>$active_url,'parent'=>$menu_id));
    if($query->num_rows() > 0){
        return 1;
    }else{
        return 0;
    }
}

function get_directory_menu($url)
{
    $ci =& get_instance();

    $ci->db->select('directory');
    $ci->db->from('menu');
    $ci->db->where('url',$url);
    $query = $ci->db->get();
    $excute = $query->result_array();
    $result = $excute[0]['directory'];
    return $result;
}

function get_name_menu($url)
{
    $ci =& get_instance();

    $ci->db->select('menu_name');
    $ci->db->from('menu');
    $ci->db->where('url',$url);
    $query = $ci->db->get();
    $excute = $query->result_array();
    $result = $excute[0]['menu_name'];
    return $result;
}

function access_menu($url)
{   
    $ci =& get_instance();
    $group_id = $ci->session->userdata('group_id');
    
    $val = $ci->db->get_where('menu',array('url'=> $url))->row();
    $menu_id = $val->menu_id;
    $query = $ci->db->get_where('menu_access',array('menu_id'=> $menu_id,'group_id'=> $group_id));
    
    if($query->num_rows() > 0){
        $val_access = $query->row();
        $access = array(
                        'view'    => $val_access->view,
                        'read'    => $val_access->read,
                        'create'  => $val_access->create, 
                        'update'  => $val_access->update,
                        'delete'  => $val_access->delete,
                        'approve' => $val_access->approve,
                        'download' => $val_access->download,
                        'print' => $val_access->print
                    );
                
    }else{
        $access = array(
                        'view'    => '0',
                        'read'    => '0',
                        'create'  => '0', 
                        'update'  => '0',
                        'delete'  => '0',
                        'approve' => '0',
                        'download' => '0',
                        'print' => '0'
                    );
    }
    return $access;
    
}

function otorisasi_menu($group_id)
{
    $ci =& get_instance();

    $access_menu = $ci->db->get_where('menu_access',array('group_id'=>$group_id));
    
    if($access_menu->num_rows() > 0)
    {
        foreach ($access_menu->result_array() as $val_access)
        {
            $arr_data['view'][$val_access['menu_id']] = $val_access['view'];
            $arr_data['read'][$val_access['menu_id']] = $val_access['read'];
            $arr_data['create'][$val_access['menu_id']] = $val_access['create'];
            $arr_data['update'][$val_access['menu_id']] = $val_access['update'];
            $arr_data['delete'][$val_access['menu_id']] = $val_access['delete'];
            $arr_data['approve'][$val_access['menu_id']] = $val_access['approve'];
            $arr_data['download'][$val_access['menu_id']] = $val_access['download'];
            $arr_data['print'][$val_access['menu_id']] = $val_access['print'];

        }

        // $ci->db->select('*'); 
        // $ci->db->from('menu');
        // $ci->db->where('menu_id NOT IN SELECT menu_id FROM menu_access WHERE group_id=\''.$group_id.'\' ',NULL,FALSE);
        // $query = $ci->db->get(); 

        $query = $ci->db->query('SELECT * FROM menu WHERE menu_id NOT IN (SELECT menu_id FROM menu_access WHERE group_id=\''.$group_id.'\')');
        if($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $val_menu) {
                $arr_data['view'][$val_menu['menu_id']] = '0';
                $arr_data['read'][$val_menu['menu_id']] = '0';
                $arr_data['create'][$val_menu['menu_id']] = '0';
                $arr_data['update'][$val_menu['menu_id']] = '0';
                $arr_data['delete'][$val_menu['menu_id']] = '0';
                $arr_data['approve'][$val_menu['menu_id']] = '0';
                $arr_data['download'][$val_menu['menu_id']] = '0';
                $arr_data['print'][$val_menu['menu_id']] = '0';
            }
        }
    }
    else
    {
        $menu = $ci->db->get('menu');
        foreach ($menu->result_array() as $val_menu)
        {
            $arr_data['view'][$val_menu['menu_id']] = "";
            $arr_data['read'][$val_menu['menu_id']] = "";
            $arr_data['create'][$val_menu['menu_id']] = "";
            $arr_data['update'][$val_menu['menu_id']] = "";
            $arr_data['delete'][$val_menu['menu_id']] = "";
            $arr_data['approve'][$val_menu['menu_id']] = "";
            $arr_data['download'][$val_menu['menu_id']] = "";
            $arr_data['print'][$val_menu['menu_id']] = "";
        }
    }
    
    $ci->db->select('*');
    $ci->db->from('menu');
    $ci->db->where('parent','0');
    $ci->db->where('status','1');
    $ci->db->order_by('order','ASC');
    $query = $ci->db->get();
    $result = $query->result_array();
    $no = 1;
    foreach ($result as $val_parent)
    {
        $view_checked = ($arr_data['view'][$val_parent['menu_id']] == '1') ? "checked":"";
        $read_checked = ($arr_data['read'][$val_parent['menu_id']] == '1') ? "checked":"";
        $create_checked = ($arr_data['create'][$val_parent['menu_id']] == '1') ? "checked":"";
        $update_checked = ($arr_data['update'][$val_parent['menu_id']] == '1') ? "checked":"";
        $delete_checked = ($arr_data['delete'][$val_parent['menu_id']] == '1') ? "checked":"";
        $approve_checked = ($arr_data['approve'][$val_parent['menu_id']] == '1') ? "checked":"";
        $download_checked = ($arr_data['download'][$val_parent['menu_id']] == '1') ? "checked":"";
        $print_checked = ($arr_data['print'][$val_parent['menu_id']] == '1') ? "checked":"";
        echo "<tr>";
            echo "<td><b>".$val_parent['menu_alias']."</b></td>";
            echo "<td><input type='hidden' name='menu_id[]' value='".$val_parent['menu_id']."'><input type='checkbox' name='view[".$val_parent['menu_id']."]' ".$view_checked."></td>";
            echo "<td><input type='checkbox' name='read[".$val_parent['menu_id']."]' ".$read_checked."></td>";
            echo "<td><input type='checkbox' name='create[".$val_parent['menu_id']."]' ".$create_checked."></td>";
            echo "<td><input type='checkbox' name='update[".$val_parent['menu_id']."]' ".$update_checked."></td>";
            echo "<td><input type='checkbox' name='delete[".$val_parent['menu_id']."]' ".$delete_checked."></td>";
            echo "<td><input type='checkbox' name='approve[".$val_parent['menu_id']."]'".$approve_checked."></td>";
            echo "<td><input type='checkbox' name='download[".$val_parent['menu_id']."]'".$download_checked."></td>";
            echo "<td><input type='checkbox' name='print[".$val_parent['menu_id']."]'".$print_checked."></td>";
        echo "</tr>";

                $first_childs = otorisasi_menu_childs($val_parent['menu_id']);
                    foreach ($first_childs as $val_first_child )
                    {
                        $view_checked1 = ($arr_data['view'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $read_checked1 = ($arr_data['read'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $create_checked1 = ($arr_data['create'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $update_checked1 = ($arr_data['update'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $delete_checked1 = ($arr_data['delete'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $approve_checked1 = ($arr_data['approve'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $download_checked1 = ($arr_data['download'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        $print_checked1 = ($arr_data['print'][$val_first_child['menu_id']] == '1') ? "checked":"";
                        echo "<tr>";
                            echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$val_first_child['menu_alias']."</td>";
                            echo "<td><input type='hidden' name='menu_id[]' value='".$val_first_child['menu_id']."'><input type='checkbox' name='view[".$val_first_child['menu_id']."]' ".$view_checked1."></td>";
                            echo "<td><input type='checkbox' name='read[".$val_first_child['menu_id']."]' ".$read_checked1."></td>";
                            echo "<td><input type='checkbox' name='create[".$val_first_child['menu_id']."]' ".$create_checked1."></td>";
                            echo "<td><input type='checkbox' name='update[".$val_first_child['menu_id']."]' ".$update_checked1."></td>";
                            echo "<td><input type='checkbox' name='delete[".$val_first_child['menu_id']."]' ".$delete_checked1."></td>";
                            echo "<td><input type='checkbox' name='approve[".$val_first_child['menu_id']."]' ".$approve_checked1."></td>";
                            echo "<td><input type='checkbox' name='download[".$val_first_child['menu_id']."]'".$download_checked1."></td>";
                            echo "<td><input type='checkbox' name='print[".$val_first_child['menu_id']."]'".$print_checked1."></td>";
                        echo "</tr>";
              
                            $second_childs = otorisasi_menu_childs($val_first_child['menu_id']); 
                                foreach ($second_childs as $val_second_child)
                                {
                                    $view_checked2 = ($arr_data['view'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $read_checked2 = ($arr_data['read'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $create_checked2 = ($arr_data['create'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $update_checked2 = ($arr_data['update'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $delete_checked2 = ($arr_data['delete'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $approve_checked2 = ($arr_data['approve'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $download_checked2 = ($arr_data['download'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    $print_checked2 = ($arr_data['print'][$val_second_child['menu_id']] == '1') ? "checked":"";
                                    echo "<tr>";
                                        echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$val_second_child['menu_alias']."</td>";
                                        echo "<td><input type='hidden' name='menu_id[]' value='".$val_second_child['menu_id']."'><input type='checkbox' name='view[".$val_second_child['menu_id']."]' ".$view_checked2."></td>";
                                        echo "<td><input type='checkbox' name='read[".$val_second_child['menu_id']."]' ".$read_checked2."></td>";
                                        echo "<td><input type='checkbox' name='create[".$val_second_child['menu_id']."]' ".$create_checked2."></td>";
                                        echo "<td><input type='checkbox' name='update[".$val_second_child['menu_id']."]' ".$update_checked2."></td>";
                                        echo "<td><input type='checkbox' name='delete[".$val_second_child['menu_id']."]'".$delete_checked2."></td>";
                                        echo "<td><input type='checkbox' name='approve[".$val_second_child['menu_id']."]' ".$approve_checked2."></td>";
                                        echo "<td><input type='checkbox' name='download[".$val_second_child['menu_id']."]' ".$download_checked2."></td>";
                                        echo "<td><input type='checkbox' name='print[".$val_second_child['menu_id']."]' ".$print_checked2."></td>";
                                    echo "</tr>";

                                        $third_childs = otorisasi_menu_childs($val_second_child['menu_id']); 
                                            foreach ($third_childs as $val_third_child)
                                            {
                                                $view_checked3 = ($arr_data['view'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $read_checked3 = ($arr_data['read'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $create_checked3 = ($arr_data['create'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $update_checked3 = ($arr_data['update'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $delete_checked3 = ($arr_data['delete'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $approve_checked3 = ($arr_data['approve'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $download_checked3 = ($arr_data['download'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                $print_checked3 = ($arr_data['print'][$val_third_child['menu_id']] == '1') ? "checked":"";
                                                echo "<tr>";
                                                    echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$val_second_child['menu_alias']."</td>";
                                                    echo "<td><input type='hidden' name='menu_id[]' value='".$val_third_child['menu_id']."'><input type='checkbox' name='view[".$val_third_child['menu_id']."]' ".$view_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='read[".$val_third_child['menu_id']."]' ".$read_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='create[".$val_third_child['menu_id']."]' ".$create_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='update[".$val_third_child['menu_id']."]' ".$update_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='delete[".$val_third_child['menu_id']."]' ".$delete_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='approve[".$val_third_child['menu_id']."]' ".$approve_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='download[".$val_third_child['menu_id']."]' ".$download_checked3."></td>";
                                                    echo "<td><input type='checkbox' name='print[".$val_third_child['menu_id']."]' ".$print_checked3."></td>";
                                                echo "</tr>";
                                            }
                                }
                    }
        
        $no++;  
    }
}

function otorisasi_menu_childs($parent_id)
{
    $ci =& get_instance();

    $ci->db->select('*');
    $ci->db->from('menu');
    $ci->db->where('parent',$parent_id);
    $ci->db->where('status','1');
    $ci->db->order_by('order','ASC');
    $query = $ci->db->get();
    $result = $query->result_array();

    return $result;
}

function breadcrumb($directory)
{
    $dir = explode("/",$directory);
    $crumb = "<div class='page-header-breadcrumb'>";
    $crumb .= "<ul class='breadcrumb-title'>";
    for($i = 1; $i < count($dir)-1; $i++ )
    {
        $crumb .= "<li class='breadcrumb-item'><a href='#!'>".$dir[$i]."</a></li>";
    }
    $crumb .= "</div>";
    echo $crumb;
}

function new_fiture($file_name)
{
    echo "";
}
function dump($array)
{
    echo "<pre>";print_r($array);
    echo "</pre>";
}

function asd($array)
{
    echo "<pre>";print_r($array);
    echo "</pre>";
    die();
}

?>